﻿namespace DevTask
{
    public static class SharedDataAnnotationErrorMessage
    {
        public const string RequiredErrorMessageRequiredErrorMessage = "required";
        public const string RequiredErrorMessage = "This field is required";
        public const string InvalidEmailErrorMessage = "Please enter a valid email address";
        public const string InvalidPasswordConfirmationErrorMessage = "Password and confirmed password are not matched";
        public const string InvalidNumber = "Please enter a valid number value";
        public const string NotAllowedValue = "This value isn't allowed";
        public const string InvalidGuid = "Please enter a valid guid value";
        public const string InvalidBoolean = "Please enter a valid bool value";
        public const string InvalidDateTimeFormat = "Please enter a valid datetime format";
    }
}
