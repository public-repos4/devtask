﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace DevTask
{
    public class JqueryDatatable
    {
        #region Request jqueryDatatable properties
        public int Draw { get; set; }
        public int Start { get; set; }
        public int Length { get; set; }
        public int Order { get; set; }
        public string SortColumn { get; set; }
        public int SortColumnIndex { get; set; }
        public string SortColumnDir { get; set; }
        public string Search { get; set; }
        public int PageSize { get; set; }
        public int Skip { get; set; }
        #endregion

        #region Response jqueryDatatable properties
        public int TotalRecords { get; set; }
        public int TotalFilteredRecords { get; set; }
        #endregion

        public IDictionary<string, string> ColsSearchValues;
        public List<object> data;

        public JqueryDatatable(HttpRequest request)
        {
            ColsSearchValues = new Dictionary<string, string>();
            data = new List<object>();

            Draw = Convert.ToInt16(request.Form["draw"]);
            Start = Convert.ToInt16(request.Form["start"]);
            Length = Convert.ToInt16(request.Form["length"]);
            Order = Convert.ToInt16(request.Form["order[0][column]"]);
            SortColumn = request.Form["columns[" + Convert.ToInt16(request.Form["order[0][column]"]) + "][name]"];
            SortColumnIndex = Convert.ToInt16(request.Form["order[0][column]"]);
            SortColumnDir = request.Form["order[0][dir]"];
            Search = request.Form["search[value]"].ToString().ToLower();
            var totalColumns = (request.Form.Count - 7) / 6;

            for (int i = 0; i < totalColumns; i++)
            {
                if ((string.IsNullOrEmpty(request.Form["columns[" + i + "][name]"]))) continue;
                ColsSearchValues.Add(request.Form["columns[" + i + "][name]"], request.Form["columns[" + i + "][search][value]"].ToString().ToLower());
            }
        }
    }
}
