using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using DevTask.DataAccess.Infrastructure.IInfrastructure;
using DevTask.DataAccess.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Http;
using System;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using DevTask.DataAccess.Helper.CutomValidation;
using DevTask.Middleware;
using DevTask.Models.Identity;

namespace DevTask
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(options =>
            {
                //Apply authorize attribute globally
                var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();
                options.Filters.Add(new AuthorizeFilter(policy));
            })
            .SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
            .AddJsonOptions(options =>
            {
                // Use the default property (Pascal) casing.
                options.JsonSerializerOptions.PropertyNamingPolicy = null;
            })

            //Newtonsoft.Json
            .AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            });

            services.AddControllersWithViews();
            services.AddControllersWithViews().AddRazorRuntimeCompilation();

            services.AddAutoMapper(typeof(Startup));

            services.AddDbContext<AppDbContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("DevTaskDbConnection"));
            });

            services.AddIdentity<User, Role>(options =>
            {
                options.Password.RequiredLength = 1;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireDigit = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
            }).AddEntityFrameworkStores<AppDbContext>();

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(60);
            });

            #region Configure JWT Token Authentication
            services.AddAuthentication(auth =>
            {
                auth.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                auth.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(token =>
            {
                token.RequireHttpsMetadata = false;
                token.SaveToken = true;
                token.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration.GetSection("SSK").Value)),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
            #endregion

            services.AddAuthorization(options =>
            {
                options.AddPolicy("AddingProducts", policy => policy.RequireClaim("AddProducts"));
                options.AddPolicy("ReadingProducts", policy => policy.RequireClaim("ReadProducts"));
                options.AddPolicy("EditingProducts", policy => policy.RequireClaim("EditProducts"));
                options.AddPolicy("DeletingProducts", policy => policy.RequireClaim("DeleteProducts"));

                options.AddPolicy("AddingCategories", policy => policy.RequireClaim("AddCategories"));
                options.AddPolicy("ReadingCategories", policy => policy.RequireClaim("ReadCategories"));
                options.AddPolicy("EditingCategories", policy => policy.RequireClaim("EditCategories"));
                options.AddPolicy("DeletingCategories", policy => policy.RequireClaim("DeleteCategories"));
            });

            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseGlobalExceptionMiddleware();

            #region Session
            app.UseSession();
            #endregion

            //Add JWToken to all incoming HTTP Request Header
            app.Use(async (context, next) =>
            {
                var path = context.Request.Path.Value;
                var jwToken = context.Session.GetString("JWToken");

                if (!string.IsNullOrEmpty(jwToken) && !context.Request.Headers.ContainsKey("Authorization"))
                {
                    context.Request.Headers.Add("Authorization", "Bearer " + jwToken);
                }
                await next();
            });

            app.UseWhen(context => context.Request.Path.StartsWithSegments("/api"), appBuilder =>
            {
                appBuilder.UseStatusCodePagesWithReExecute("/api/Errors/Error/{0}");
            });

            app.UseWhen(context => !context.Request.Path.StartsWithSegments("/api"), appBuilder =>
            {
                appBuilder.UseStatusCodePagesWithReExecute("/Errors/Error/{0}");
            });

            #region Https redirection
            app.UseHttpsRedirection();
            #endregion

            #region Static files
            app.UseStaticFiles();
            #endregion

            #region Cookie policy
            //app.UseCookiePolicy();
            #endregion

            #region Routing
            // Matches request to an endpoint.
            app.UseRouting();
            #endregion

            #region CORS (Configures Cross-Origin Resource Sharing)
            // app.UseCors();
            #endregion

            #region Authentication
            // Cheking if the user is exist
            app.UseAuthentication();
            #endregion

            #region Authorization
            // Cheking if the user has a valid role to the app resources
            app.UseAuthorization();
            #endregion

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                name: "default",
                pattern: "{controller=Products}/{action=Index}/{id?}"
                );
            });
        }
    }
}
