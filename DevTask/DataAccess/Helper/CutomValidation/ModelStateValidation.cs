﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Reflection;

namespace DevTask.DataAccess.Helper.CutomValidation
{
    public enum ErrorType
    {
        InvalidModelState = 1,
    }
    public class InvalidModelProperty
    {
        public string Property { get; set; }
        public IEnumerable<string> ErrorMsgs { get; set; }
    }

    public class ModelStateValidation : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                object model = context.ActionArguments.Any()
                    ? context.ActionArguments.First().Value
                    : null;

                List<InvalidModelProperty> invalidProperties = new List<InvalidModelProperty>();

                foreach (var item in context.ModelState)
                {
                    if (item.Value.ValidationState == Microsoft.AspNetCore.Mvc.ModelBinding.ModelValidationState.Invalid)
                    {
                        PropertyInfo propertyInfo = model.GetType().GetProperties().Where(prop => prop.Name == item.Key).First();

                        Type propType = propertyInfo.PropertyType;

                        invalidProperties.Add(new InvalidModelProperty
                        {
                            Property = item.Key,
                            ErrorMsgs = propType switch
                            {
                                _ when propType == typeof(Guid) || propType == typeof(Guid?) => new List<string> { SharedDataAnnotationErrorMessage.InvalidGuid },
                                _ when propType == typeof(DateTime) || propType == typeof(DateTime?) => new List<string> { SharedDataAnnotationErrorMessage.InvalidDateTimeFormat },
                                _ when propType == typeof(Boolean) || propType == typeof(Boolean?) => new List<string> { SharedDataAnnotationErrorMessage.InvalidBoolean },
                                _ => item.Value.Errors.Select(error => error.ErrorMessage).ToList()
                            }
                        });
                    }
                }

                context.Result = new BadRequestObjectResult(new
                {
                    errorType = ((ErrorType)ErrorType.InvalidModelState).ToString(),
                    InvalidProperties = invalidProperties
                });
            }
        }
    }
}
