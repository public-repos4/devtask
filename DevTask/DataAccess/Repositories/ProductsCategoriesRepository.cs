﻿using Microsoft.EntityFrameworkCore;

using DevTask.DataAccess.Repositories.IRepositories;
using DevTask.Models;
using System.Threading.Tasks;
using System.Collections.Generic;
using DevTask.GetParams;
using DevTask.DataAccess.Infrastructure;
using System.Linq;
using System;

namespace DevTask.DataAccess.Repositories
{
    public class ProductsCategoriesRepository : GenericRepository<ProductCategory>, IProductsCategoriesRepository
    {
        public ProductsCategoriesRepository(AppDbContext context) : base(context)
        {

        }
    }
}
