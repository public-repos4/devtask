﻿using Microsoft.EntityFrameworkCore;

using DevTask.DataAccess.Repositories.IRepositories;
using DevTask.Models;
using System.Threading.Tasks;
using System.Collections.Generic;
using DevTask.GetParams;
using DevTask.DataAccess.Infrastructure;
using System.Linq;
using System;

namespace DevTask.DataAccess.Repositories
{
    public class ProductsRepository : GenericRepository<Product>, IProductsRepository
    {
        public ProductsRepository(AppDbContext context) : base(context)
        {

        }

        public async Task<EntityList<Product>> Get(ProductParams productParams)
        {
            IQueryable<Product> products = _ctxt.Set<Product>()
            .Include(product => product.ProductCategories)
            .ThenInclude(productCategory => productCategory.Category);

            int totalCount = products.Count();

            if (!string.IsNullOrEmpty(productParams.Id.ToString()))
            {
                products = products.Where(product => product.Id == productParams.Id);
            }

            if (productParams.Ids != null)
            {
                products = products.Where(category => productParams.Ids.Contains(category.Id));
            }

            if (!string.IsNullOrEmpty(productParams.Search))
            {
                products = products.Where(product =>
                   product.Name.Contains(productParams.Search)
                || product.Description.Contains(productParams.Search)
                || product.Price.ToString().Contains(productParams.Search)
                );
            }

            if (!string.IsNullOrEmpty(productParams.Name))
            {
                products = products.Where(product => product.Name == productParams.Name);
            }

            if (!string.IsNullOrEmpty(productParams.Description))
            {
                products = products.Where(product => product.Description == productParams.Description);
            }

            if (productParams.Price != null)
            {
                products = products.Where(product => product.Price == productParams.Price);
            }

            if (productParams.CategoriesIds != null)
            {
                IEnumerable<Guid> productsIds = _ctxt.ProductsCategories.Where(productCategory => productParams.CategoriesIds.Contains(productCategory.CategoryId)).Select(y => y.ProductId).ToList();
                products = products.Where(product => productsIds.Contains(product.Id));
            }

            products = GetOrderBy(productParams.SortCol, productParams.SortDir)(products);

            int? pageNumber = productParams.PageNumber;
            int? pageSize = productParams.PageSize;

            if (pageNumber != null && pageSize != null)
            {
                if (pageNumber == -1) //return the last page
                {
                    products = products
                        .Skip(Math.Max(0, products.Count() - products.Count() % pageSize.Value))
                        .Take(products.Count() % pageSize.Value);
                }
                else //return a custom page
                {
                    int skip = (pageNumber.Value - 1) * pageSize.Value;
                    products = products.Skip(skip).Take(pageSize.Value);
                }
            }

            return new EntityList<Product>
            {
                TotalCount = totalCount,
                FilteredCount = products.Count(),
                FilteredData = await products.ToListAsync(),
            };
        }
    }
}
