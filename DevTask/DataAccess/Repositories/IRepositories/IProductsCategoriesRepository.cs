﻿using DevTask.GetParams;
using DevTask.Models;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DevTask.DataAccess.Repositories.IRepositories
{
    public interface IProductsCategoriesRepository : IGenericRepository<ProductCategory>
    {
    }
}
