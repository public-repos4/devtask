﻿using DevTask.DataAccess.Infrastructure;
using DevTask.GetParams;
using DevTask.Models;
using System.Threading.Tasks;

namespace DevTask.DataAccess.Repositories.IRepositories
{
    public interface IProductsRepository : IGenericRepository<Product>
    {
        //Task<IEnumerable<Product>> Get(ProductParams productParams);
        Task<EntityList<Product>> Get(ProductParams productParams);
    }
}
