﻿using DevTask.DataAccess.Infrastructure;
using DevTask.GetParams;
using DevTask.Models;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DevTask.DataAccess.Repositories.IRepositories
{
    public interface ICategoriesRepository : IGenericRepository<Category>
    {
        Task<EntityList<Category>> Get(CategoryParams categoryParams);
    }
}
