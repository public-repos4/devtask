﻿using System;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using AutoMapper;
using Microsoft.EntityFrameworkCore.Query;
using DevTask.DataAccess.Infrastructure;
using System.Reflection;

namespace DevTask.DataAccess.Repositories.IRepositories
{
    public interface IGenericRepository<T> where T : class
    {
        void Add(T entity);
        void Add(IEnumerable<T> entities);

        //Task<IEnumerable<T>> Get(
        //    Expression<Func<T, bool>> filter = null,
        //    Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
        //    string includeProperties = null
        //    );

        Task<EntityList<T>> Get(
                 Expression<Func<T, bool>> filter = null,
                 //Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
                 string orderColumns = null,
                 string orderType = null,
                 string includeProperties = null,
                 int? pageNumber = null,
                 int? pageSize = null);

        void Update(T entity);
        void Update(IEnumerable<T> entities);

        void Delete(Guid id);
        void Delete(T entity);
        public void Delete(IEnumerable<T> entities);
    }
}
