﻿using System;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using AutoMapper;
using DevTask.DataAccess.Repositories.IRepositories;
using Microsoft.EntityFrameworkCore.Query;
using DevTask.DataAccess.Infrastructure;
using System.Reflection;

namespace DevTask.DataAccess.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        protected readonly AppDbContext _ctxt;

        public GenericRepository(AppDbContext context)
        {
            _ctxt = context;
        }

        public void Add(T entity)
        {
            _ctxt.Set<T>().Add(entity);
        }

        public void Add(IEnumerable<T> entities)
        {
            _ctxt.Set<T>().AddRange(entities);
        }

        public async Task<IEnumerable<T>> Get(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = null)
        {
            IQueryable<T> query = _ctxt.Set<T>();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            //include properties will be comma seperated
            if (includeProperties != null)
            {
                foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }
            }

            if (orderBy != null)
            {
                return await orderBy(query).ToListAsync();
            }
            return await query.ToListAsync();
        }

        public async Task<EntityList<T>> Get(
            Expression<Func<T, bool>> filter = null,
            //Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string orderColumns = null,
            string orderType = null,
            string includeProperties = null,
            int? pageNumber = null,
            int? pageSize = null)
        {
            IQueryable<T> query = _ctxt.Set<T>();

            int totalCount = query.Count();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            //include properties will be comma seperated
            if (includeProperties != null)
            {
                foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }
            }

            //if (orderBy != null)
            //{
            //    query = orderBy(query);
            //}

            if (orderColumns != null && orderType != null)
            {
                query = GetOrderBy(orderColumns, orderType)(query);
            }

            if (pageNumber != null && pageSize != null)
            {
                if (pageNumber == -1) //return the last page
                {
                    query = query
                        .Skip(Math.Max(0, query.Count() - query.Count() % pageSize.Value))
                        .Take(query.Count() % pageSize.Value);
                }
                else //return a custom page
                {
                    int skip = (pageNumber.Value - 1) * pageSize.Value;
                    query = query.Skip(skip).Take(pageSize.Value);
                }
            }

            return new EntityList<T>
            {
                TotalCount = totalCount,
                FilteredCount = query.Count(),
                FilteredData = await query.ToListAsync(),
            };
        }

        public void Update(T entity)
        {
            _ctxt.ChangeTracker.Clear();
            _ctxt.Set<T>().Update(entity);
            _ctxt.Entry(entity).State = EntityState.Modified;
        }
        public void Update(IEnumerable<T> entities)
        {
            _ctxt.ChangeTracker.Clear();
            _ctxt.Set<T>().UpdateRange(entities);
        }

        public void Delete(Guid id)
        {
            T entityToRemove = _ctxt.Set<T>().Find(id);
            Delete(entityToRemove);
        }

        public void Delete(T entity)
        {
            _ctxt.Set<T>().Remove(entity);
        }

        public void Delete(IEnumerable<T> entities)
        {
            _ctxt.Set<T>().RemoveRange(entities);
        }

        public Func<IQueryable<T>, IOrderedQueryable<T>> GetOrderBy(string orderColumns, string orderType)
        {
            Type typeQueryable = typeof(IQueryable<T>);
            ParameterExpression argQueryable = Expression.Parameter(typeQueryable, "p");
            var outerExpression = Expression.Lambda(argQueryable, argQueryable);
            string[] props = orderColumns.Split('.');
            IQueryable<T> query = new List<T>().AsQueryable<T>();
            Type type = typeof(T);
            ParameterExpression arg = Expression.Parameter(type, "x");

            Expression expr = arg;
            IEnumerable<string> typePropsNames = type.GetProperties().Select(prop => prop.Name.ToLower());
            foreach (string prop in props)
            {
                if (!typePropsNames.Contains(prop.ToLower())) continue;

                PropertyInfo pi = type.GetProperty(prop, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                expr = Expression.Property(expr, pi);
                type = pi.PropertyType;
            }
            LambdaExpression lambda = Expression.Lambda(expr, arg);
            string methodName = (orderType == "desc" || orderType == "descending") ? "OrderByDescending" : "OrderBy";

            MethodCallExpression resultExp =
                Expression.Call(typeof(Queryable), methodName, new Type[] { typeof(T), type }, outerExpression.Body, Expression.Quote(lambda));
            var finalLambda = Expression.Lambda(resultExp, argQueryable);
            return (Func<IQueryable<T>, IOrderedQueryable<T>>)finalLambda.Compile();
        }
    }
}
