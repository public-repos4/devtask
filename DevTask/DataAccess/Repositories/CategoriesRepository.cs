﻿using Microsoft.EntityFrameworkCore;

using DevTask.DataAccess.Repositories.IRepositories;
using DevTask.Models;
using System.Threading.Tasks;
using System.Collections.Generic;
using DevTask.GetParams;
using DevTask.DataAccess.Infrastructure;
using System.Linq;
using System;

namespace DevTask.DataAccess.Repositories
{
    public class CategoriesRepository : GenericRepository<Category>, ICategoriesRepository
    {
        public CategoriesRepository(AppDbContext context) : base(context)
        {

        }

        public async Task<EntityList<Category>> Get(CategoryParams categoryParams)
        {
            IQueryable<Category> categories = _ctxt.Set<Category>()
            .Include(category => category.CategoryProducts)
            .ThenInclude(categoryProduct => categoryProduct.Product);

            int totalCount = categories.Count();

            if (!string.IsNullOrEmpty(categoryParams.Id.ToString()))
            {
                categories = categories.Where(category => category.Id == categoryParams.Id);
            }

            if (categoryParams.Ids != null)
            {
                categories = categories.Where(category => categoryParams.Ids.Contains(category.Id));
            }

            if (!string.IsNullOrEmpty(categoryParams.Search))
            {
                categories = categories.Where(category => category.Name.Contains(categoryParams.Search));
            }

            if (!string.IsNullOrEmpty(categoryParams.Name))
            {
                categories = categories.Where(category => category.Name == categoryParams.Name);
            }

            if (categoryParams.ProductsIds != null)
            {
                IEnumerable<Guid> categoriesIds = _ctxt.ProductsCategories.Where(productCategory => categoryParams.ProductsIds.Contains(productCategory.ProductId)).Select(y => y.CategoryId).ToList();
                if (categoriesIds != null)
                {
                    categories = categories.Where(category => categoriesIds.Contains(category.Id));
                }
            }

            categories = GetOrderBy(categoryParams.SortCol, categoryParams.SortDir)(categories);

            int? pageNumber = categoryParams.PageNumber;
            int? pageSize = categoryParams.PageSize;

            if (pageNumber != null && pageSize != null)
            {
                if (pageNumber == -1) //return the last page
                {
                    categories = categories
                        .Skip(Math.Max(0, categories.Count() - categories.Count() % pageSize.Value))
                        .Take(categories.Count() % pageSize.Value);
                }
                else //return a custom page
                {
                    int skip = (pageNumber.Value - 1) * pageSize.Value;
                    categories = categories.Skip(skip).Take(pageSize.Value);
                }
            }

            return new EntityList<Category>
            {
                TotalCount = totalCount,
                FilteredCount = categories.Count(),
                FilteredData = await categories.ToListAsync(),
            };
        }
    }
}
