﻿using AutoMapper;
using DevTask.DataAccess.Repositories.IRepositories;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevTask.DataAccess.Infrastructure.IInfrastructure
{
    public interface IUnitOfWork : IDisposable
    {
        IProductsRepository Products { get; }
        ICategoriesRepository Categories { get; }
        IProductsCategoriesRepository ProductsCategories { get; }

        void Commit();
    }
}
