﻿using System;
using DevTask.DataAccess.Infrastructure.IInfrastructure;
using DevTask.DataAccess.Repositories.IRepositories;
using DevTask.DataAccess.Repositories;
using Microsoft.AspNetCore.Identity;
using DevTask.Models.Identity;
using AutoMapper;

namespace DevTask.DataAccess.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool _disposed;

        private ICategoriesRepository _categories;
        private IProductsRepository _products;
        private IProductsCategoriesRepository _productsCategories;

        private readonly AppDbContext _dbContext;
        private readonly UserManager<User> _usersManager;
        private readonly RoleManager<Role> _rolesManager;

        public UnitOfWork(AppDbContext dbContext,
            IMapper mapper,
            UserManager<User> usersManager,
            RoleManager<Role> rolesManager
            )
        {
            _dbContext = dbContext;
            _usersManager = usersManager;
            _rolesManager = rolesManager;
        }

        public ICategoriesRepository Categories
        {
            get { return _categories ?? (_categories = new CategoriesRepository(_dbContext)); }
        }

        public IProductsRepository Products
        {
            get { return _products ?? (_products = new ProductsRepository(_dbContext)); }
        }

        public IProductsCategoriesRepository ProductsCategories
        {
            get { return _productsCategories ?? (_productsCategories = new ProductsCategoriesRepository(_dbContext)); }
        }

        public void Commit()
        {
            try
            {
                _dbContext.SaveChanges();
                //_transaction.Commit();
            }
            catch
            {
                throw;
            }
            finally
            {
                ResetRepositories();
            }
        }

        private void ResetRepositories()
        {
            _products = null;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    //if (_transaction != null)
                    //{
                    //    _transaction.Dispose();
                    //    _transaction = null;
                    //}
                    //if (_connection != null)
                    //{
                    //    _connection.Dispose();
                    //    _connection = null;
                    //}
                }
                _disposed = true;
            }
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
    }
}