﻿using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

using DevTask.Models.Identity;
using System.Security.Claims;
using System.Collections.Generic;
using System.Linq;
using DevTask.Models;

namespace DevTask.DataAccess.Infrastructure
{
    public class Seed
    {
        private const string AdminUserId = "B3E6A8BC-80C9-481D-8915-A57259C158E7";
        private const string WajeehUserId = "187D831C-9AFD-442C-81A0-262F9B631430";

        public async static System.Threading.Tasks.Task SeedUsers(RoleManager<Role> rolesManager, UserManager<User> usersManager, AppDbContext appDbContext)
        {
            try
            {
                appDbContext.Database.OpenConnection();

                if (!rolesManager.Roles.Any())
                {
                    var roles = new List<Role>
                {
                    new Role{Name = "Admin"},
                    new Role{Name = "User"},
                };

                    foreach (var role in roles)
                    {
                        rolesManager.CreateAsync(role).Wait();
                    }

                    await appDbContext.SaveChangesAsync();
                }

                if (!usersManager.Users.Any())
                {
                    var adminUser = new User
                    {
                        Id = new Guid(AdminUserId),
                        FirstName = "admin",
                        LastName = "admin",
                        UserName = "admin",
                        Email = "admin@task.org",
                        IsActive = true
                    };

                    var wajeehUser = new User
                    {
                        Id = new Guid(WajeehUserId),
                        FirstName = "Wajeeh",
                        LastName = "Abiad",
                        UserName = "Wajeeh",
                        Email = "wajeeh@task.org",
                        IsActive = true
                    };

                    try
                    {
                        IdentityResult result;

                        result = usersManager.CreateAsync(adminUser, "admin").Result;
                        if (result.Succeeded)
                        {
                            var admin = usersManager.FindByNameAsync(adminUser.UserName).Result;
                            usersManager.AddToRolesAsync(admin, new[] { "Admin" }).Wait();
                            usersManager.AddClaimsAsync(admin, new List<Claim>
                            {
                                new Claim("AddProducts", "True"),
                                new Claim("ReadProducts", "True"),
                                new Claim("EditProducts", "True"),
                                new Claim("DeleteProducts", "True"),

                                new Claim("AddCategories", "True"),
                                new Claim("ReadCategories", "True"),
                                new Claim("EditCategories", "True"),
                                new Claim("DeleteCategories", "True"),
                            }).Wait();
                        }

                        result = usersManager.CreateAsync(wajeehUser, "wajeeh").GetAwaiter().GetResult();
                        if (result.Succeeded)
                        {
                            var user = usersManager.FindByNameAsync(wajeehUser.UserName).GetAwaiter().GetResult();
                            usersManager.AddToRolesAsync(user, new[] { "User" }).Wait();
                        }

                        await appDbContext.SaveChangesAsync();
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }

                if (!appDbContext.Categories.Any())
                {
                    for (int i = 0; i < 1000; i++)
                    {
                        await appDbContext.Categories.AddAsync(new Category
                        {
                            Id = new Guid(),
                            Name = "Category " + (i + 1),
                            CreatedAt = DateTime.Now,
                            CreatedBy = new Guid(AdminUserId),
                            UpdatedAt = DateTime.Now,
                            UpdatedBy = new Guid(AdminUserId)
                        });
                    }

                    await appDbContext.SaveChangesAsync();
                }

                if (!appDbContext.Products.Any())
                {
                    var random = new Random();

                    for (int i = 0; i < 1000; i++)
                    {
                        await appDbContext.Products.AddAsync(new Product
                        {
                            Id = new Guid(),
                            Name = "Product " + (i + 1),
                            Description = "Description " + (i + 1),
                            Price = (float)(random.NextDouble() * (10000 - 1000) + 1000),
                            CreatedAt = DateTime.Now,
                            CreatedBy = new Guid(AdminUserId),
                            UpdatedAt = DateTime.Now,
                            UpdatedBy = new Guid(AdminUserId)
                        });
                    }

                    await appDbContext.SaveChangesAsync();
                }

                if (!appDbContext.ProductsCategories.Any())
                {
                    var random = new Random();
                    for (int i = 0; i < 1000; i++)
                    {
                        await appDbContext.ProductsCategories.AddAsync(new ProductCategory
                        {
                            Id = new Guid(),
                            ProductId = appDbContext.Products.AsEnumerable().ElementAt(random.Next(appDbContext.Products.Count())).Id,
                            CategoryId = appDbContext.Categories.AsEnumerable().ElementAt(random.Next(appDbContext.Products.Count())).Id,
                            CreatedAt = DateTime.Now,
                            CreatedBy = new Guid(AdminUserId),
                            UpdatedAt = DateTime.Now,
                            UpdatedBy = new Guid(AdminUserId)
                        });
                    }

                    await appDbContext.SaveChangesAsync();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                appDbContext.Database.CloseConnection();
            }
        }
    }
}
