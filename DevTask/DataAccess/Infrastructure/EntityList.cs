﻿using System.Collections.Generic;

namespace DevTask.DataAccess.Infrastructure
{
    public class EntityList<T>
    {
        public int TotalCount { get; set; }
        public int FilteredCount { get; set; }
        public IEnumerable<T> FilteredData { get; set; }
    }
}
