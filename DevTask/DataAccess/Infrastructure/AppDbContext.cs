﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using DevTask.Models.Identity;
using System;
using System.Linq;
using DevTask.Models;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace DevTask.DataAccess.Infrastructure
{
    public class AppDbContext :
        IdentityDbContext<User, Role, Guid,
        UserClaim, UserRole, UserLogin,
        RoleClaim, UserToken>
    {

        protected readonly IHttpContextAccessor _httpContextAccessor;

        public AppDbContext(DbContextOptions<AppDbContext> options, IHttpContextAccessor httpContextAccessor = null) : base(options)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<ProductCategory> ProductsCategories { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<UserRole>(userRole =>
            {
                userRole.HasKey(userRole => new { userRole.UserId, userRole.RoleId });

                userRole.HasOne(userRole => userRole.Role)
                    .WithMany(user => user.RoleUsers)
                    .HasForeignKey(ur => ur.RoleId)
                    .IsRequired();

                userRole.HasOne(userRole => userRole.User)
                    .WithMany(role => role.UserRoles)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired();
            });

            builder.Entity<ProductCategory>()
            .HasOne(productCategory => productCategory.Category)
            .WithMany(category => category.CategoryProducts)
            .OnDelete(DeleteBehavior.Restrict);
        }

        public override int SaveChanges()
        {
            var entries = ChangeTracker
                .Entries()
                .Where(e => e.Entity is BaseEntity && (
                        e.State == EntityState.Added
                        || e.State == EntityState.Modified));

            var userId = new Guid(_httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);

            foreach (var entityEntry in entries)
            {
                if (entityEntry.State == EntityState.Added)
                {
                    ((BaseEntity)entityEntry.Entity).CreatedAt = DateTime.Now;
                    ((BaseEntity)entityEntry.Entity).UpdatedAt = DateTime.Now;

                    ((BaseEntity)entityEntry.Entity).CreatedBy = userId;
                    ((BaseEntity)entityEntry.Entity).UpdatedBy = userId;
                }

                if (entityEntry.State == EntityState.Modified)
                {
                    entityEntry.Property("CreatedAt").IsModified = false;
                    entityEntry.Property("CreatedBy").IsModified = false;

                    ((BaseEntity)entityEntry.Entity).UpdatedAt = DateTime.Now;
                    ((BaseEntity)entityEntry.Entity).UpdatedBy = userId;
                }
            }

            return base.SaveChanges();
        }
    }
}
