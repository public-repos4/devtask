﻿namespace DevTask
{
    public class SharedExceptionErrorMessages
    {
        // 500
        public const string InternalServerError = "Some thing went wrong...";


        //LoginExceptions
        // 520
        public const string InvalidLoginAttempt = "Your login credential is incorrect. please try again";
        // 521
        public const string UnexpectedLoginFailure = "Correct credential but failed logine";
        // 522
        public const string NoLongerAvailableAccount = "Your account is no longer available";
        // 523
        public const string InvalidExternalCredential = "Please configure a correct external login credential";
        // 524
        public const string NoAccountMatchExternalCredential = "There is no local account match your external login credential";
        // 525
        public const string InvalidExternalLoginAttempt = "Your external login credential is incorrect. please try again";

        //Users CRUD exceptions
        // 526
        public const string UserNameIsInUse = "The current username is specified to another user";
        // 527
        public const string EmailIsInUse = "The current email is specified to another user";
        // 547 
        public const string ConflictedWithReferenceConstraint = "You can not delete this item to its association with other data";
        // 548 
        public const string InCorrectForeignKeys = "You specified incorrect foreign key(s)";
    }
}
