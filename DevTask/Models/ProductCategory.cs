﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DevTask.Models
{
    [Table("ProductsCategories")]
    public class ProductCategory : BaseEntity
    {
        public virtual Product Product { get; set; }
        public Guid ProductId { get; set; }

        public virtual Category Category { get; set; }
        public Guid CategoryId { get; set; }
    }
}
