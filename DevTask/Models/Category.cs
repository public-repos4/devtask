﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DevTask.Models
{
    [Table("Categories")]
    public class Category : BaseEntity
    {
        [Required]
        public string Name { get; set; }

        [InverseProperty("Category")]
        public virtual ICollection<ProductCategory> CategoryProducts { get; set; }
    }
}
