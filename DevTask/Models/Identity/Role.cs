﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace DevTask.Models.Identity
{
    public class Role : IdentityRole<Guid>
    {
        [InverseProperty("Role")]
        public virtual ICollection<UserRole> RoleUsers { get; set; }
    }
}
