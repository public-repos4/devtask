﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DevTask.Models
{
    [Table("Products")]
    public class Product : BaseEntity
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        public float Price { get; set; }

        [InverseProperty("Product")]
        public virtual ICollection<ProductCategory> ProductCategories { get; set; }
    }
}
