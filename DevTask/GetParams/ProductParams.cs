﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace DevTask.GetParams
{
    public class ProductParams : QueryStringParams
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public float? Price { get; set; }
        public IEnumerable<Guid> CategoriesIds { get; set; }
    }
}
