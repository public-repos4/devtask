﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace DevTask.GetParams
{
    public class CategoryParams : QueryStringParams
    {
        public IEnumerable<Guid> ProductsIds { get; set; }
        public string Name { get; set; }
    }
}
