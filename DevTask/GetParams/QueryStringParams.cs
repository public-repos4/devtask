﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DevTask.GetParams
{
    public abstract class QueryStringParams
    {
        const int maxPageSize = 1000;

        private int? _pageSize;
        private int? _pageNumber;
        private string _sortCol;
        private string _sortDir;

        public QueryStringParams()
        {
            _pageSize = 50;
            _pageNumber = 1;
            _sortCol = "Id";
            _sortDir = "asc";
        }

        public string Search { get; set; }

        public Guid? Id { get; set; }
        public IEnumerable<Guid> Ids { get; set; }

        public string SortCol
        {
            get => _sortCol;
            set => _sortCol = value ?? "Id";
        }
        public string SortDir
        {
            get => _sortDir;
            set => _sortDir = value ?? "asc";
        }

        public int? PageNumber
        {
            get => _pageNumber;
            set => _pageNumber = (value < 1 && value != -1) ? 1 : value; // -1 represent the last page : invalid number
        }

        public int? PageSize
        {
            get => _pageSize;
            set => _pageSize = (value > maxPageSize) ? maxPageSize : value;
        }
    }
}
