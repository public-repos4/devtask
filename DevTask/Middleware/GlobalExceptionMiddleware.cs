﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Newtonsoft.Json;
using System.Net;

namespace DevTask.Middleware
{
    public static class GlobalExceptionMiddleware
    {
        public static void UseGlobalExceptionMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionHandler>();
        }

        public class ExceptionHandler
        {
            //private readonly ILogger _logger;
            private readonly RequestDelegate _next;
            public ExceptionHandler(RequestDelegate next)
            {
                _next = next;
            }

            public async Task InvokeAsync(HttpContext httpContext)
            {
                try
                {
                    await _next(httpContext);
                }
                catch (Exception ex)
                {
                    //_logger.LogError($"Something went wrong: {ex.Message}");
                    await HandleGlobalExceptionAsync(httpContext, ex);
                }
            }

            private static Task HandleGlobalExceptionAsync(HttpContext context, Exception exception)
            {
                context.Response.ContentType = "application/json";
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                string errorMsg;
                bool isDevelopment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";

                    //A custom exception was thrown
                    string customExceptionCode = exception.Message;

                    #region Shared custom exceptions
                    List<string> sharedCustomExceptionCodes = new() { "526", "527", "547", "548" };

                    if (sharedCustomExceptionCodes.Contains(customExceptionCode))
                    {
                        errorMsg = customExceptionCode switch
                        {
                            "526" => SharedExceptionErrorMessages.UserNameIsInUse,
                            "527" => SharedExceptionErrorMessages.EmailIsInUse,
                            "547" => SharedExceptionErrorMessages.ConflictedWithReferenceConstraint,
                            "548" => SharedExceptionErrorMessages.InCorrectForeignKeys,
                            _ => isDevelopment ? exception.Message : SharedExceptionErrorMessages.InternalServerError,
                        };

                        return context.Response.WriteAsync(JsonConvert.SerializeObject(new
                        {
                            StatusCode = context.Response.StatusCode,
                            Message = errorMsg
                        }));
                    }
                    #endregion

                    #region Api custom exceptions
                    else if (context.Request.Path.StartsWithSegments(new PathString("/api")))
                    {
                        errorMsg = customExceptionCode switch
                        {
                            "520" => SharedExceptionErrorMessages.InvalidLoginAttempt,
                            "521" => SharedExceptionErrorMessages.UnexpectedLoginFailure,
                            _ => isDevelopment ? exception.Message : SharedExceptionErrorMessages.InternalServerError,
                        };

                        return context.Response.WriteAsync(JsonConvert.SerializeObject(new
                        {
                            StatusCode = context.Response.StatusCode,
                            Message = errorMsg
                        }));
                    }
                    #endregion

                    #region Mvc custom exceptions
                    else
                    {
                        List<string> loginCustomExceptionCodes = new() { "520", "521", "522", "523", "524", "525" };

                        if (loginCustomExceptionCodes.Contains(customExceptionCode))
                        {
                            switch (customExceptionCode)
                            {
                                case "520":
                                    context.Response.Cookies.Append("loginExceptionMessage", SharedExceptionErrorMessages.InvalidLoginAttempt);
                                    break;

                                case "521":
                                    context.Response.Cookies.Append("loginExceptionMessage", SharedExceptionErrorMessages.UnexpectedLoginFailure);
                                    break;

                                //External login exceptions
                                case "522":
                                    context.Response.Cookies.Append("loginExceptionMessage", SharedExceptionErrorMessages.NoLongerAvailableAccount);
                                    break;

                                case "523":
                                    context.Response.Cookies.Append("loginExceptionMessage", SharedExceptionErrorMessages.InvalidExternalCredential);
                                    break;

                                case "524":
                                    context.Response.Cookies.Append("loginExceptionMessage", SharedExceptionErrorMessages.NoAccountMatchExternalCredential);
                                    break;

                                case "525":
                                    context.Response.Cookies.Append("loginExceptionMessage", SharedExceptionErrorMessages.InvalidExternalLoginAttempt);
                                    break;

                                    //default:
                                    //    return context.Response.WriteAsync(JsonConvert.SerializeObject(new
                                    //    {
                                    //        StatusCode = context.Response.StatusCode,
                                    //        Message = exception.Message + exception.StackTrace
                                    //    }));
                            }
                            context.Response.Redirect("/");

                            return Task.Delay(0);
                        }

                        return context.Response.WriteAsync(JsonConvert.SerializeObject(new
                        {
                            StatusCode = context.Response.StatusCode,
                            Message = isDevelopment
                            ? string.Format("Error message is: \n {0}\n\n Error stack trace is: \n {1}", exception.Message, exception.StackTrace)
                            : SharedExceptionErrorMessages.InternalServerError,
                        }));
                    }
                    #endregion

                }
            }
    }
}
