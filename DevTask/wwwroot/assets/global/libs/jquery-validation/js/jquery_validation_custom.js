﻿$(document).on('keyup keydown change', 'form input[jquery-validation-required], form textarea[jquery-validation-required]', function () {
    validate_elem($(this));
});

$(document).on('change', 'form select[jquery-validation-required]', function () {
    validate_elem($(this));
});

function isFormValid($form) {
    validateForm($form);

    return $form.valid();
}

function validateForm($form) {
    $.validator.unobtrusive.parse($form);
    $form.validate();

    var validator = $form.validate()
    validator.errorList

    $form.find('.form-control[jquery-validation-required]').each(function (index, element) {
        validate_elem($(element));
    });
}

function validate_elem($elem) {
    $elem.valid() ? set_elem_valid($elem) : set_elem_invalid($elem);
}

function reset_form($form) {
    $form.removeAttr('already-validated');

    $form.find('input[data-clear!="false"]').val("");
    $form.find('textarea[data-clear!="false"]').val("");
    $form.find('select[data-clear!="false"]').val("");
    $form.find('select[data-clear!="false"]').trigger('change');
    $form.find('input[type="checkbox"][data-clear!="false"]').prop('checked', false);
    //$form.find('input[type="checkbox"][data-clear!="false"]').prop('disabled', true);

    $form.find('> div span.validation-error').empty();
    //$form.find('.form-control').removeClass('validation-error valid');
    $form.find('.valid').removeClass('valid');
    $form.find('.validation-error').removeClass('validation-error');

    $form.find('.form-control[jquery-validation-required]').each(function (index, element) {
        reset_elem($(element));
    });
}

function reset_elem($elem) {
    let $form_group_elem = $elem.parents('.form-group');
    $form_group_elem.find('.form-control').removeClass('border-success border-danger valid validation-error');
    $form_group_elem.find('> div > div.form-control-feedback').removeClass('text-success text-danger');
    $form_group_elem.find('> div > div.form-control-feedback i').removeClass('icon-checkmark-circle icon-cancel-circle2 text-muted');

    if ($elem.hasAttr('has-icon')) {
        $form_group_elem.find('> div > div.form-control-feedback i').addClass('text-muted');
    }
}

function set_elem_valid($elem) {
    reset_elem($elem);

    let $form_group_elem = $elem.parents('.form-group');
    // $parent_form_group_elem.find('label').addClass('text-success');
    $form_group_elem.find('.form-control').addClass('border-success');
    $form_group_elem.find('> div > div.form-control-feedback').addClass('text-success');

    if ($elem.hasAttr('has-icon')) {
        //Has icon
        $form_group_elem.find('> div > div.form-control-feedback i').removeClass('text-muted');

    } else {
        //Doesn't have icon
        $form_group_elem.find('> div > div.form-control-feedback i').addClass('icon-checkmark-circle');
    }

    $('.picker').removeClass('text-success'); //to reset date picker to default style
}

function set_elem_invalid($elem) {
    reset_elem($elem);

    let $form_group_elem = $elem.parents('.form-group');
    $form_group_elem.find('.form-control').addClass('border-danger');
    $form_group_elem.find('> div > div.form-control-feedback').addClass('text-danger');

    if ($elem.hasAttr('has-icon')) {
        //Has icon
        $form_group_elem.find('> div > div.form-control-feedback i').removeClass('text-muted');

    } else {
        //Doesn't have icon
        $form_group_elem.find('> div > div.form-control-feedback i').addClass('icon-cancel-circle2');
    }

    $('.picker').removeClass('text-danger'); //to reset date picker to default style
}




