﻿$.validator.setDefaults({
    ignore: '',
    errorElement: 'span',
    errorClass: 'validation-error',

    errorPlacement: function (error, element) {
        var placement = $(element).closest('.form-group').find('span.error-msg');
        if (placement) {
            $(placement).append(error)
        } else {
            error.insertAfter(element);
        }
    }
});

$(function ($) {
    $.validator.addMethod("requiredif", function (value, element, parameters) {
        parameters['desiredValue'] = $(element).data('desired-value');
        parameters['seedProperty'] = $(element).data('seed-property');
        var seedValue = $(`input[data-title="${parameters['seedProperty']}"]`).attr('data-value');

        if ((seedValue == parameters.desiredValue) && !(value))
            return false;
        else
            return true;
    });
}(jQuery));


//https://johnnycode.com/2014/03/27/using-jquery-validate-plugin-html5-data-attribute-rules/

//(Tested, core)
//data-rule-required="true"
//data-rule-email="true"

//(Untested, core, but should work)
//data-rule-url="true"
//data-rule-date="true"
//data-rule-dateISO="true"
//data-rule-number="true"
//data-rule-digits="true"
//data-rule-creditcard="true"
//data-rule-minlength="6"
//data-rule-maxlength="24"
//data-rule-rangelength="5,10"
//data-rule-min="5"
//data-rule-max="10"
//data-rule-range="5,10"
//data-rule-equalto="#password"
//data-rule-remote="custom-validatation-endpoint.aspx"

//(Untested, additional, but should work)
//data-rule-accept=""
//data-rule-bankaccountNL="true"
//data-rule-bankorgiroaccountNL="true"
//data-rule-bic=""
//data-rule-cifES=""
//data-rule-creditcardtypes=""
//data-rule-currency=""
//data-rule-dateITA=""
//data-rule-dateNL=""
//data-rule-extension=""
//data-rule-giroaccountNL=""
//data-rule-iban=""
//data-rule-integer="true"
//data-rule-ipv4="true"
//data-rule-ipv6="true"
//data-rule-mobileNL=""
//data-rule-mobileUK=""
//data-rule-lettersonly="true"
//data-rule-nieES=""
//data-rule-nifES=""
//data-rule-nowhitespace="true"
//data-rule-pattern=""
//data-rule-phoneNL="true"
//data-rule-phoneUK="true"
//data-rule-phoneUS="true"
//data-rule-phonesUK="true"
//data-rule-postalcodeNL="true"
//data-rule-postcodeUK="true"
//data-rule-require_from_group=""
//data-rule-skip_or_fill_minimum=""
//data-rule-strippedminlength=""
//data-rule-time=""
//data-rule-time12h=""
//data-rule-url2=""
//data-rule-vinUS=""
//data-rule-zipcodeUS="true"
//data-rule-ziprange=""


