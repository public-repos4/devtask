// English

$.extend($.fn.dataTable.defaults, {
    //language: {
    //    customTranslation: {
    //        //<editor-fold desc="Custom translation">
    //        copy: 'Copy',
    //        colvis: 'Columns Visibility',
    //        print: 'Print',
    //        printAll: 'Print all',
    //        printSelected: 'Print selected',
    //        control: 'Control',
    //        download: 'Download',
    //        pdf: 'Download as .pdf',
    //        csv: 'Download as .csv',
    //        excel: 'Download as .xlsx',
    //        //</editor-fold>
    //    }
    //},

    //buttons: [
    //    {
    //        extend: 'collection',
    //        text: '<i class="fa fa-ellipsis-v"></i>',
    //        className: 'btn p-2',
    //        buttons: [
    //            {
    //                //text: '<i class="fa fa-copy"></i>' + table.translation.customTranslation.copy,
    //                text: '<i class="fa fa-copy"></i>' + 'Copy',
    //                extend: 'copyHtml5',
    //            },
    //            {
    //                //text: '<i class="fa fa-eye"></i><i class="fa fa-chevron-right last-child"></i>' + table.translation.customTranslation.colvis,
    //                text: '<i class="fa fa-eye"></i><i class="fa fa-chevron-right last-child"></i>' + 'Columns Visibility',
    //                extend: 'colvis',
    //                columns: ':not(.noVis)'
    //            },
    //            {
    //                extend: 'collection',
    //                //text: '<i class="mi-print font-20"></i><i class="fa fa-chevron-right last-child"></i>' + table.translation.customTranslation.print,
    //                text: '<i class="mi-print font-20"></i><i class="fa fa-chevron-right last-child"></i>' + 'Print',
    //                buttons: [
    //                    {
    //                        extend: 'print',
    //                        //text: '<i class="fa fa-tasks"></i>' + table.translation.customTranslation.printAll,
    //                        text: '<i class="fa fa-tasks"></i>' + 'Print all',
    //                        exportOptions: {
    //                            columns: ':visible',
    //                        }
    //                    },
    //                    {
    //                        extend: 'print',
    //                        //text: '<i class="fa fa-check"></i>' + table.translation.customTranslation.printSelected,
    //                        text: '<i class="fa fa-check"></i>' + 'Print selected',
    //                        exportOptions: {
    //                            columns: ':visible',
    //                            modifier: {
    //                                selected: true
    //                            }
    //                        }
    //                    }
    //                ],
    //            },
    //            {
    //                extend: 'collection',
    //                //text: '<i class="mi-settings font-20"></i>' + table.translation.customTranslation.control + '<i class="fa fa-chevron-right last-child"></i>',
    //                text: '<i class="mi-settings font-20"></i>' + 'Control' + '<i class="fa fa-chevron-right last-child"></i>',
    //                buttons: [
    //                    { extend: 'selectAll' },
    //                    { extend: 'selectNone' },
    //                    { extend: 'selectRows' },
    //                    { extend: 'selectColumns' },
    //                    { extend: 'selectCells' }
    //                ]
    //            },
    //            {
    //                extend: 'collection',
    //                //text: '<i class="fa fa-download"></i>' + table.translation.customTranslation.download + '<i class="fa fa-chevron-right last-child"></i>',
    //                text: '<i class="fa fa-download"></i>' + 'Download' + '<i class="fa fa-chevron-right last-child"></i>',
    //                buttons: [
    //                        //text: '<i class="fa fa-file-pdf"></i>' + table.translation.customTranslation.pdf,
    //                    {
    //                        text: '<i class="fa fa-file-pdf"></i>' + 'Download as .pdf',
    //                        extend: 'pdfHtml5',
    //                        filename: 'Data export'
    //                    },
    //                    {
    //                        //text: '<i class="fa fa-file-csv"></i>' + table.translation.customTranslation.csv,
    //                        text: '<i class="fa fa-file-csv"></i>' + 'Download as .csv',
    //                        extend: 'csvHtml5',
    //                        filename: 'Data export'
    //                    },
    //                    {
    //                        //text: '<i class="fa fa-file-excel"></i>' + table.translation.customTranslation.excel,
    //                        text: '<i class="fa fa-file-excel"></i>' + 'Download as .xlsx',
    //                        extend: 'excelHtml5',
    //                        filename: 'Data export'
    //                    }
    //                ],
    //            }
    //        ]
    //    }
    //],
});