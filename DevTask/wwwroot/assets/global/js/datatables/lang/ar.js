 //Arabic

$(document).ready(function () {
    jQuery.extend(jQuery.fn.dataTable.defaults, {
        language: {
            sEmptyTable: 'ليست هناك بيانات متاحة في الجدول',
            sLoadingRecords: 'جارٍ التحميل...',
            sProcessing: 'جارٍ التحميل...',
            sLengthMenu: '<span>أظهر</span> _MENU_ ',
            sZeroRecords: 'لم يعثر على أية سجلات',
            sInfo: 'إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخلات',
            sInfoEmpty: 'يعرض 0 إلى 0 من أصل 0 سجل',
            sInfoFiltered: '(منتقاة من مجموع _MAX_ مُدخل)',
            sInfoPostFix: '',
            sSearch: '<span>ابحث:</span>',
            searchPlaceholder: 'اكتب للبحث...',
            sUrl: '',
            oPaginate: {
                sFirst: 'الأول',
                sPrevious: 'السابق',
                sNext: 'التالي',
                sLast: 'الأخير'
            },
            oAria: {
                sSortAscending: ': تفعيل لترتيب العمود تصاعدياً',
                sSortDescending: ': تفعيل لترتيب العمود تنازلياً'
            },
            select: {
                rows: {
                    _: '%d أسطر محددة',
                    0: 'انقر لتحديد سطر',
                    1: 'سطر واحد محدد',
                    2: 'سطران محددان'
                }
            },
            buttons: {
                selectAll: 'تحديد جميع الأسطر',
                selectNone: 'إلغاء تحديد جميع الأسطر',
                selectRows: 'تحديد أسطر',
                selectColumns: 'تحديد أعمدة',
                selectCells: 'تحديد خلايا',
            },
            //customTranslation: {
            //    //<editor-fold desc="Custom translation">
            //    copy: 'نسخ',
            //    colvis: 'عرض الأعمدة',
            //    print: 'طباعة',
            //    printAll: 'طباعة الكل',
            //    printSelected: 'طباعة المحدد',
            //    control: 'تحكم',
            //    download: 'تحميل',
            //    pdf: 'تحميل كـ pdf.',
            //    csv: 'تحميل كـ csv.',
            //    excel: 'تحميل كـ xlsx.',
            //    //</editor-fold>
            //}
        },
        //buttons: [
        //    {
        //        dom: 'Bfrtip',
        //        extend: 'collection',
        //        text: '<i class="fa fa-ellipsis-v"></i>',
        //        className: 'btn p-2',
        //        buttons: [
        //            {
        //                //text: '<i class="fa fa-copy"></i>' + table.translation.customTranslation.copy,
        //                text: '<i class="fa fa-copy"></i>' + 'نسخ',
        //                extend: 'copyHtml5',
        //            },
        //            {
        //                //text: '<i class="fa fa-eye"></i><i class="fa fa-chevron-right last-child"></i>' + table.translation.customTranslation.colvis,
        //                text: '<i class="fa fa-eye"></i><i class="fa fa-chevron-right last-child"></i>' + 'عرض الأعمدة',
        //                extend: 'colvis',
        //                columns: ':not(.noVis)'
        //            },
        //            {
        //                extend: 'collection',
        //                //text: '<i class="mi-print font-20"></i><i class="fa fa-chevron-right last-child"></i>' + table.translation.customTranslation.print,
        //                text: '<i class="mi-print font-20"></i><i class="fa fa-chevron-right last-child"></i>' + 'طباعة',
        //                buttons: [
        //                    {
        //                        extend: 'print',
        //                        //text: '<i class="fa fa-tasks"></i>' + table.translation.customTranslation.printAll,
        //                        text: '<i class="fa fa-tasks"></i>' + 'طباعة الكل',
        //                        exportOptions: {
        //                            columns: ':visible',
        //                        }
        //                    },
        //                    {
        //                        extend: 'print',
        //                        //text: '<i class="fa fa-check"></i>' + table.translation.customTranslation.printSelected,
        //                        text: '<i class="fa fa-check"></i>' + 'طباعة المحدد',
        //                        exportOptions: {
        //                            columns: ':visible',
        //                            modifier: {
        //                                selected: true
        //                            }
        //                        }
        //                    }
        //                ],
        //            },
        //            {
        //                extend: 'collection',
        //                //text: '<i class="mi-settings font-20"></i>' + table.translation.customTranslation.control + '<i class="fa fa-chevron-right last-child"></i>',
        //                text: '<i class="mi-settings font-20"></i>' + 'تحكم' + '<i class="fa fa-chevron-right last-child"></i>',
        //                buttons: [
        //                    { extend: 'selectAll' },
        //                    { extend: 'selectNone' },
        //                    { extend: 'selectRows' },
        //                    { extend: 'selectColumns' },
        //                    { extend: 'selectCells' }
        //                ]
        //            },
        //            {
        //                extend: 'collection',
        //                //text: '<i class="fa fa-download"></i>' + table.translation.customTranslation.download + '<i class="fa fa-chevron-right last-child"></i>',
        //                text: '<i class="fa fa-download"></i>' + 'تحميل' + '<i class="fa fa-chevron-right last-child"></i>',
        //                buttons: [
        //                    {
        //                        //text: '<i class="fa fa-file-pdf"></i>' + table.translation.customTranslation.pdf,
        //                        text: '<i class="fa fa-file-pdf"></i>' + 'تحميل كـ pdf.',
        //                        extend: 'pdfHtml5',
        //                        filename: 'Data export'
        //                    },
        //                    {
        //                        //text: '<i class="fa fa-file-csv"></i>' + table.translation.customTranslation.csv,
        //                        text: '<i class="fa fa-file-csv"></i>' + 'تحميل كـ csv.',
        //                        extend: 'csvHtml5',
        //                        filename: 'Data export'
        //                    },
        //                    {
        //                        //text: '<i class="fa fa-file-excel"></i>' + table.translation.customTranslation.excel,
        //                        text: '<i class="fa fa-file-excel"></i>' + 'تحميل كـ xlsx.',
        //                        extend: 'excelHtml5',
        //                        filename: 'Data export'
        //                    }
        //                ],
        //            }
        //        ]
        //    }
        //],
    });
});
