﻿$(document).ready(function () {

    //#region Categories table CRUD
    const $createCategory = $('#createCategory');
    const $editCategory = $('#editCategory');

    //#region Categories table CRUD (Create)
    $(document).on('click', '[data-title="openAddCategoryForm"]', function () {
        $createCategory.modal('show');
    });

    $createCategory.on('show.bs.modal', function () {
        $createCategory.find('.btn-submit').prop('disabled', false);
    });

    $createCategory.on('keydown', function (event) {
        if (event.which === 13) {
            event.preventDefault();
            $(this).find('[data-title="postCategory"]').click();
        }
    });

    $createCategory.on('click', '[data-title="postCategory"]', function () {
        if ($(this).prop('disabled')) return;

        saveFormInfo($createCategory, {
            method: $createCategory.attr('method'),
            url: $createCategory.attr('action'),
            data: new FormData($(this).closest('form')[0]),
            processData: false,
            contentType: false,
            $blocks: [$createCategory.find('.block').first()],
        }).then(function () {
            $categoriesDataTable.ajax.reload();
        });

    });

    $createCategory.on('hidden.bs.modal', function () {
        clearFormData($(this));
    });
    //#endregion

    //#region Categories table CRUD (Edit)
    $(document).on('click', '[data-title="openEditCategoryForm"]', function () {
        fillEditCategoryForm($(this).closest("tr"), $editCategory);
        $editCategory.modal('show');
    });

    $editCategory.on('show.bs.modal', function () {
        $editCategory.find('.btn-submit').prop('disabled', false);
    });

    $editCategory.on('keydown', function (event) {
        if (event.which === 13) {
            event.preventDefault();
            $(this).find('[data-title="putCategory"]').click();
        }
    });

    $editCategory.on('click', '[data-title="putCategory"]', function () {
        if ($(this).prop('disabled')) return;

        saveFormInfo($editCategory, {
            method: $editCategory.attr('method'),
            url: $editCategory.attr('action') + $editCategory.find('input[name="id"]').val(),
            data: new FormData($(this).closest('form')[0]),
            processData: false,
            contentType: false,
            $blocks: [$editCategory.find('.block').first()],
        }).then(function () {
            $categoriesDataTable.ajax.reload();
        });
    });

    $editCategory.on('hidden.bs.modal', function () {
        clearFormData($(this));
    });
    //#endregion

    //#region Categories table CRUD (Delete)
    $(document).on('click', '[data-title="openDeleteCategoryConfirm"]', function () {
        $('#deleteConfirmationModal').find('.btn-delete').attr('data-title', 'deleteCategory')

        $('#deleteConfirmationModal').find('.modal-body').find('h4').text(
            $(this).closest('tr').children('td[data-title="name"]').text()
        );
        $('#deleteConfirmationModal').modal('show');

        categoryId = $(this).closest('tr').children('td[data-title="name"]').attr('data-entity-id');
    });

    $(document).on('click', '[data-title="deleteCategory"]', function () {
        if ($(this).prop('disabled')) return;
        $(this).prop('disabled', true);

        deleteEntities($('#deleteConfirmationModal'), {
            method: 'Delete',
            url: '/Categories/' + categoryId,
        }).then(function () {
            $categoriesDataTable.ajax.reload();
        });
    });
    //#endregion

    //#endregion
});

//<editor-fold desc="Categories table CRUD (Edit)">
function fillEditCategoryForm($category, $form) {
    $.each($form.find('input, textarea'), function (index, element) {
        $(element).val($category.find('[data-title="' + $(element).attr('data-title') + '"]').text());
    });

    $.each($form.find('select.remote-data'), function (index, element) {
        $(element).attr('ignore-change-event', true);
        $(element).data('value', $category.find('[data-title="' + $(element).attr('data-title') + '"]').data('value')).select2('open').select2('close');
    });

    $.each($form.find('select:not(.remote-data)'), function (index, element) {
        $(element).val($category.find('[data-title="' + $(element).attr('data-title') + '"]').data('value')).trigger('change');
    });

    $form.find('input[data-title="id"]').val($category.find('[data-title="name"]').attr("data-entity-id"));
}
//</editor-fold>