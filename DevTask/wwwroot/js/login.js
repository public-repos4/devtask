﻿$(document).ready(function () {
    const $login = $('#login');

    let loginExceptionMessage = $.cookie('loginExceptionMessage');
    if (loginExceptionMessage) {
        $.jGrowl(loginExceptionMessage, {
            theme: 'alert-styled-left bg-danger',
            position: 'bottom-left',
            life: 10000,
            //sticky: true,   // The message will stick to the screen until it is intentionally closed by the user.
        });
        $.removeCookie('loginExceptionMessage');
    }

    $login.on('keydown', function (event) {
        if (event.which === 13) {
            event.preventDefault();
            $(this).find('button[data-title="submit"]').click();
        }
    });

    $login.on('click', 'button.btn-submit', function () {
        validateForm($login)
    });
});
