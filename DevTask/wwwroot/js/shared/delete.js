﻿$(document).ready(function () {
    $deleteConfirmationModal = $('#deleteConfirmationModal');

    $deleteConfirmationModal.on('show.bs.modal', function () {
        $deleteConfirmationModal.find('.btn-delete').prop('disabled', false);
    });

    $deleteConfirmationModal.on('keydown', function (event) {
        if (event.which === 13) {
            event.preventDefault();
            $('.btn-delete').click();
        }
    });
});

function deleteEntities($deleteConfirmationModal, ajax) {
    $deleteConfirmationModal.find('.btn-delete').prop('disabled', true);

    return ajaxRequestAsObj(ajax).then(function (response) {
        $deleteConfirmationModal.modal('hide');
        $.jGrowl(response.message, {
            theme: 'bg-teal alert-success alert-styled-left',
            position: 'bottom-left',
            life: 2000,
        });
        return response;

    }).catch(function (error) {
        $deleteConfirmationModal.find('.btn-delete').prop('disabled', false);
        return error;
    });
}