﻿function saveFormInfo($form, ajax) {
    disabledAttr($form, '.btn-submit', true);

    $form.attr('already-validated', true);

    if (isFormValid($form)) {
        return ajaxRequestAsObj(ajax).then(function (response) {
            $form.modal('hide');
            $.jGrowl(response.message, {
                theme: 'bg-teal alert-success alert-styled-left',
                position: 'bottom-left',
                life: 2000,
            });
            return response;
        }).catch(function (error) {
            disabledAttr($form, '.btn-submit', false);
            return error;
        });
    } else {
        disabledAttr($form, '.btn-submit', false);
    }
}

function disabledAttr($form, $selector, disabledStatus) {
    $form.find($selector).prop('disabled', disabledStatus);
}

function clearFormData($form) {
    reset_form($form);
}

function ajaxRequestAsObj(ajax) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            headers: ajax.headers,
            method: ajax.method,
            url: ajax.url,
            data: ajax.data,
            dataType: ajax.dataType,
            processData: ajax.processData,
            contentType: ajax.contentType,
            traditional: ajax.traditional,
            cache: ajax.cache,
            async: ajax.async,

            beforeSend: function () {
                $.each(ajax.$blocks, function (index, $elem) {
                    $elem.block({
                        message: '<i class="icon-sync spinner" style="font-size: 20px;"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.5,
                            cursor: 'wait',
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'none',
                            width: '100%',
                            top: '50%',
                            zIndex: 2000,
                        },
                        //onBlock: function () {
                        //    $elem.prop('disabled', 'disabled');
                        //}
                    });
                    $('.blockUI.blockMsg').css('top', '50%');
                    //}
                });
            },
            success: function (response) {
                resolve(response);
                return true;
            },
            error: function (error) {
                console.log(error);
                reject(error);

                switch (ajax.method) {
                    case 'Post':
                    case 'Put':
                        if (error.responseJSON.errorType == 'InvalidModelState') {
                            //ErrorType.InvalidModelState

                            let invalidProperties = error.responseJSON.InvalidProperties;
                            $(invalidProperties).each(function (index, element) {
                                $form.find('[name="' + element.Property + '"]')
                                    .siblings('[data-title="validation"]')
                                    .html('<span id="' + element.Property + '-error">' + element.ErrorMsgs.join('<br />') + '</span>')
                            });
                        }
                        else {
                            $.jGrowl(JSON.parse(error.responseText).Message, {
                                theme: 'alert-styled-left bg-danger',
                                position: 'bottom-left',
                                //life: 5000,
                                sticky: true,   // The message will stick to the screen until it is intentionally closed by the user.
                            });
                        }
                        break;

                    default: // [Get, Delete] methods
                        $.jGrowl(JSON.parse(error.responseText).Message, {
                            theme: 'alert-styled-left bg-danger',
                            position: 'bottom-left',
                            life: 5000,
                            //sticky: true,   // The message will stick to the screen until it is intentionally closed by the user.
                        });
                }

            },
            complete: function () {
                $.each(ajax.$blocks, function (index, $elem) {
                    $elem.unblock();
                });
            }
        });
    });
}