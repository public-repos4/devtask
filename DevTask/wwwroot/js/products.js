﻿$(document).ready(function () {
    const categoriesPageSize = 100;

    //#region Products table CRUD
    const $createProduct = $('#createProduct');
    const $editProduct = $('#editProduct');

    //#region Products table CRUD (Create)
    $createProduct.find('select[data-title="categoriesIds"]').select2({
        placeholder: 'Choose a product',
        dropdownParent: $createProduct.find('select[data-title=categoriesIds]').closest('.form-group'),

        ajax: {
            method: 'Get',
            url: '/Categories/GetSelect',
            dataType: 'json',
            data: function (params) {
                return {
                    name: params.term,
                    id: $(this).data('value'),

                    pageNumber: params.page || 1,
                    pageSize: categoriesPageSize,
                };
            },
            success: function (response) {

            },
            complete: function () {
                //console.log('Completed');
            },

            processResults: function (response, params) {
                return {
                    results: response.data,
                    pagination: {
                        more: response.thereIsMoreData
                    }
                };
            }
        },

        templateResult: function formatState(option) {
            if (!option.id) {
                return option.text;
            }
            return $(option.optHtml);
        },

        templateSelection: function formatState(option) {
            if (!option.id) {
                return option.text;
            }
            else {
                if (option.selectedOptHtml) return $(option.selectedOptHtml)
                else if (option.element) return $(option.element.selectedOptHtml)
            }
        }
    });

    $(document).on('click', '[data-title="openAddProductForm"]', function () {
        $createProduct.modal('show');
    });

    $createProduct.on('show.bs.modal', function () {
        $createProduct.find('.btn-submit').prop('disabled', false);
    });

    $createProduct.on('keydown', function (event) {
        if (event.which === 13) {
            event.preventDefault();
            $(this).find('[data-title="postProduct"]').click();
        }
    });

    $createProduct.on('click', '[data-title="postProduct"]', function () {
        if ($(this).prop('disabled')) return;

        saveFormInfo($createProduct, {
            method: $createProduct.attr('method'),
            url: $createProduct.attr('action'),
            data: new FormData($(this).closest('form')[0]),
            processData: false,
            contentType: false,
            $blocks: [$createProduct.find('.block').first()],
        }).then(function () {
            $productsDataTable.ajax.reload();
        });

    });

    $createProduct.on('hidden.bs.modal', function () {
        clearFormData($(this));
    });
    //#endregion

    //#region Products table CRUD (Edit)
    $editProduct.find('select[data-title="categoriesIds"]').select2({
        placeholder: 'Choose a product',
        dropdownParent: $editProduct.find('select[data-title=categoriesIds]').closest('.form-group'),

        ajax: {
            method: 'Get',
            url: '/Categories/GetSelect',
            dataType: 'json',
            data: function (params) {
                return {
                    name: params.term,

                    pageNumber: params.page || 1,
                    pageSize: categoriesPageSize,
                };
            },
            success: function (response) {

                $element = $editProduct.find('select[data-title="categoriesIds"]')

                if ($editProduct.find('select[data-title="categoriesIds"]').prop('multiple')) {
                    var ids = $element.data('value');
                    if (ids) {
                        ajaxRequestAsObj({
                            method: 'Get',
                            url: '/Categories/GetSelect',
                            data: {
                                ids: ids.split(','),
                            },
                            traditional: true
                        }).then(function (response) {
                            $element = $editProduct.find('select[data-title="categoriesIds"]')

                            // Set the value, creating a new option if necessary
                            var idsArray = ids.split(',');

                            $.each(idsArray, function (index, id) {
                                //idsArray.find(x => x.id === id)
                                if (!$element.find("option[value='" + id + "']").length) {
                                    // Create a DOM Option and pre-select by default
                                    option = response.data.find(x => x.id === id);
                                    var newOption = new Option(option.text, option.id, true, true);
                                    newOption.id = option.id
                                    newOption.text = option.text
                                    newOption.optHtml = option.optHtml
                                    newOption.selectedOptHtml = option.selectedOptHtml

                                    // Append it to the select
                                    $element.append(newOption);
                                }
                            });
                            $element.val(idsArray).trigger('change');

                            $element.data('value', '');
                            $element.removeAttr('ignore-change-event');
                        });
                    }
                }
                else {
                    var id = $element.data('value');
                    if (id) {
                        ajaxRequestAsObj({
                            method: 'Get',
                            url: '/Categories/GetSelect',
                            data: {
                                id: id,
                            },
                        }).then(function (response) {
                            $element = $editProduct.find('select[data-title="categoriesIds"]')

                            // Set the value, creating a new option if necessary
                            if ($element.find("option[value='" + id + "']").length) {
                                $element.val(id).trigger('change');
                            } else {
                                // Create a DOM Option and pre-select by default
                                var newOption = new Option(response.data[0].text, response.data[0].id, true, true);
                                newOption.id = response.data[0].id
                                newOption.text = response.data[0].text
                                newOption.optHtml = response.data[0].optHtml
                                newOption.selectedOptHtml = response.data[0].selectedOptHtml

                                // Append it to the select
                                $element.append(newOption).trigger('change');
                            }

                            $element.data('value', '');
                            $element.removeAttr('ignore-change-event');
                        });
                    }
                }
            },
            complete: function () {
                //console.log('Completed');
            },

            processResults: function (response, params) {
                return {
                    results: response.data,
                    pagination: {
                        more: response.thereIsMoreData
                    }
                };
            }
        },

        templateResult: function formatState(option) {
            if (!option.id) {
                return option.text;
            }
            return $(option.optHtml);
        },

        templateSelection: function formatState(option) {
            if (!option.id) {
                return option.text;
            }
            else {
                if (option.selectedOptHtml) return $(option.selectedOptHtml)
                else if (option.element) return $(option.element.selectedOptHtml)
            }
        }
    });

    $(document).on('click', '[data-title="openEditProductForm"]', function () {
        fillEditProductForm($(this).closest("tr"), $editProduct);
        $editProduct.modal('show');
    });

    $editProduct.on('show.bs.modal', function () {
        $editProduct.find('.btn-submit').prop('disabled', false);
    });

    $editProduct.on('keydown', function (event) {
        if (event.which === 13) {
            event.preventDefault();
            $(this).find('[data-title="putProduct"]').click();
        }
    });

    $editProduct.on('click', '[data-title="putProduct"]', function () {
        if ($(this).prop('disabled')) return;

        saveFormInfo($editProduct, {
            method: $editProduct.attr('method'),
            url: $editProduct.attr('action') + $editProduct.find('input[name="id"]').val(),
            data: new FormData($(this).closest('form')[0]),
            processData: false,
            contentType: false,
            $blocks: [$editProduct.find('.block').first()],
        }).then(function () {
            $productsDataTable.ajax.reload();
        });
    });

    $editProduct.on('hidden.bs.modal', function () {
        clearFormData($(this));
    });
    //#endregion

    //#region Products table CRUD (Delete)
    $(document).on('click', '[data-title="openDeleteProductConfirm"]', function () {
        $('#deleteConfirmationModal').find('.btn-delete').attr('data-title', 'deleteProduct')

        $('#deleteConfirmationModal').find('.modal-body').find('h4').text(
            $(this).closest('tr').children('td[data-title="name"]').text()
        );
        $('#deleteConfirmationModal').modal('show');

        productId = $(this).closest('tr').children('td[data-title="name"]').attr('data-entity-id');
    });

    $(document).on('click', '[data-title="deleteProduct"]', function () {
        if ($(this).prop('disabled')) return;
        $(this).prop('disabled', true);

        deleteEntities($('#deleteConfirmationModal'), {
            method: 'Delete',
            url: '/Products/' + productId,
        }).then(function () {
            $productsDataTable.ajax.reload();
        });
    });
    //#endregion

    //#endregion
});

//<editor-fold desc="Products table CRUD (Edit)">
function fillEditProductForm($product, $form) {
    $.each($form.find('input, textarea'), function (index, element) {
        $(element).val($product.find('[data-title="' + $(element).attr('data-title') + '"]').text());
    });

    $.each($form.find('select.remote-data'), function (index, element) {
        $(element).attr('ignore-change-event', true);
        $(element).data('value', $product.find('[data-title="' + $(element).attr('data-title') + '"]').data('value')).select2('open').select2('close');
    });

    $.each($form.find('select:not(.remote-data)'), function (index, element) {
        $(element).val($product.find('[data-title="' + $(element).attr('data-title') + '"]').data('value')).trigger('change');
    });

    $form.find('input[data-title="id"]').val($product.find('[data-title="name"]').attr("data-entity-id"));
}
//</editor-fold>