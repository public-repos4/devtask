﻿namespace DevTask
{
    public static class SharedDataAnnotationRegexes
    {
        public const string FloatRegex = "[+-]?([0-9]*[.])?[0-9]+";
        public const string GuidRegex = "(?im)^[{(]?[0-9A-F]{8}[-]?(?:[0-9A-F]{4}[-]?){3}[0-9A-F]{12}[)}]?$";
        public const string DefaultDateTimeFormat = "0[1-9]|[12][0-9]|3[01]$";  //dd-mm-yyyy
    }
}
