﻿using AutoMapper;
using DevTask.Dtos.Category;
using DevTask.Dtos.Product;
using DevTask.Models;
using System.Linq;

namespace DevTask
{
    class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<AddProductDto, Product>();
            CreateMap<Product, GetProductDto>()
                .ForMember(dto => dto.Categories, c => c.MapFrom(c => c.ProductCategories.Select(cs => cs.Category)));
            CreateMap<EditProductDto, Product>(); 
            
            CreateMap<AddCategoryDto, Category>();
            CreateMap<Category, GetCategoryDto>();
            CreateMap<EditCategoryDto, Category>();
        }
    }
}
