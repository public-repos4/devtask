﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Security.Claims;
using DevTask.ActionResults;
using DevTask.Dtos.Auth;
using DevTask.Models.Identity;

namespace DevTask.Controllers
{
    public class AccountController : Controller
    {
        private readonly IConfiguration _config;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        private readonly api.AccountController _accountApi;

        public AccountController(IConfiguration config, UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _config = config;
            _userManager = userManager;
            _signInManager = signInManager;

            _accountApi = new api.AccountController(_config, _userManager, _signInManager);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Login")]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/Login")]
        public IActionResult Login([FromForm] UserCredentialDto userCredentialDto)
        {
            try
            {
                LoginApiActionResult LoginApiActionResult = (LoginApiActionResult)((OkObjectResult)_accountApi.Login(userCredentialDto).Result).Value;
                HttpContext.Session.SetString("JWToken", LoginApiActionResult.Token);

                string redirectTo = Request.Cookies["redirectTo"];
                Response.Cookies.Delete("redirectTo");

                if (Url.IsLocalUrl(redirectTo))
                {
                    return Redirect(redirectTo);
                }
                return Redirect("/");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.Message);
            }
        }

        // GET: Auth/Logout
        [HttpGet]
        [Route("/Logout")]
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return Redirect("/Login");
        }

    }
}
