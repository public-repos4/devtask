﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;

namespace DevTask.Controllers.api
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    public class ErrorsController : ControllerBase
    {
        //[HttpGet, HttpPost, HttpPut, HttpDelete]  //We can use this
        [AcceptVerbs("Get", "Post", "Put", "Delete")]   //Or this
        [Route("Error/{StatusCode}")]
        public async Task ErrorsHandler(string statusCode)
        {
            string errorMessage = statusCode switch
            {
                "400" => "Bad request",
                "401" => "Unauthorized user",
                "403" => "Not allowed",
                "404" => "Not found",
                "405" => "Method Not Allowed",
                //"500" => "Sorry, something when wrog",
                _ => "Sorry, something when wrog",
            };

            Response.ContentType = "application/json";

            await Response.WriteAsync(JsonConvert.SerializeObject(new
            {
                StatusCode = statusCode,
                Message = errorMessage
            }));
        }
    }
}