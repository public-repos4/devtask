﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DevTask.DataAccess.Infrastructure.IInfrastructure;
using AutoMapper;
using DevTask.ActionResults;
using System.Net;
using Microsoft.AspNetCore.Authorization;
using DevTask.Dtos.Category;
using DevTask.Models;
using DevTask.GetParams;

namespace DevTask.Controllers.api
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class CategoriesController : BaseController
    {
        public CategoriesController(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {

        }

        // POST: api/Categories
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Route("")]
        [Authorize(Policy = "AddingCategories")]
        public ActionResult Post([FromForm] AddCategoryDto addCategoryDto)
        {
            Category category = _mapper.Map<Category>(addCategoryDto);

            _unitOfWork.Categories.Add(category);

            _unitOfWork.Commit();

            return Get(new CategoryParams
            {
                Id = category.Id
            });
        }

        // GET: api/Categories
        [HttpGet]
        [Route("")]
        [Authorize(Policy = "ReadingCategories")]
        public ActionResult Get([FromQuery] CategoryParams categoryParams)
        {
            var entitiesList = _unitOfWork.Categories.Get(categoryParams).GetAwaiter().GetResult();

            return Ok(new ApiActionResult
            {
                StatusCode = HttpStatusCode.OK,
                TotalCount = entitiesList.TotalCount,
                FilteredCount = entitiesList.FilteredCount,
                Data = _mapper.Map<IEnumerable<GetCategoryDto>>(entitiesList.FilteredData),
            });
        }

        // PUT: api/Categories
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut]
        [Route("{id}")]
        [Authorize(Policy = "EditingCategories")]
        public ActionResult Put(Guid id, [FromForm] EditCategoryDto editCategoryDto)
        {
            if (id != editCategoryDto.Id)
            {
                return Redirect("/api/Errors/Error/400");
            }

            Category category = _unitOfWork.Categories.Get(category => category.Id == id).GetAwaiter().GetResult().FilteredData.FirstOrDefault();

            if (category == null)
            {
                return Redirect("/api/Errors/Error/404");
            }

            category = _mapper.Map<Category>(editCategoryDto);

            _unitOfWork.Categories.Update(category);

            _unitOfWork.Commit();

            return Get(new CategoryParams
            {
                Id = category.Id
            });
        }

        // DELETE: api/Categories/{id}
        [HttpDelete]
        [Route("{id}")]
        [Authorize(Policy = "DeletingCategories")]
        public ActionResult Delete(Guid id)
        {
            Category category = _unitOfWork.Categories.Get(category => category.Id == id).GetAwaiter().GetResult().FilteredData.FirstOrDefault();

            if (category == null)
            {
                return Redirect("/api/Errors/Error/404");
            }

            if (_unitOfWork.ProductsCategories.Get(pc => pc.CategoryId == id).GetAwaiter().GetResult().FilteredData.Any())
                throw new Exception("547"); // Entity conflicted with reference constraint

            _unitOfWork.Categories.Delete(id);

            _unitOfWork.Commit();

            return Ok(new ApiActionResult
            {
                StatusCode = HttpStatusCode.OK,
                Data = _mapper.Map<GetCategoryDto>(category)
            });
        }
    }
}
