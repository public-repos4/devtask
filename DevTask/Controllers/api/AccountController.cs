﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DevTask.ActionResults;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.IdentityModel.Tokens.Jwt;
using DevTask.Middleware;
using DevTask.Models.Identity;
using DevTask.Dtos.Auth;

namespace DevTask.Controllers.api
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public AccountController(IConfiguration config, UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _config = config;
        }

        // POST: api/Account
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [AllowAnonymous]
        [Route("Login")]
        public async Task<ActionResult> Login([FromForm] UserCredentialDto userCredentialDto, [FromQuery] string returnUrl = null)
        {
            User appUser = await _userManager.Users.FirstOrDefaultAsync(u =>
                u.UserName == userCredentialDto.UserName.ToLower() &&
                u.IsActive
           );

            if (appUser == null)
                throw new Exception("520"); // Incorrect credential (details: incorrect Username)

            var signingInResult = await _signInManager.PasswordSignInAsync(userCredentialDto.UserName, userCredentialDto.Password, false, false);

            if (!signingInResult.Succeeded)
                throw new Exception("520"); // Incorrect credential (details: correct Username & incorrect password)

            return Ok(new LoginApiActionResult
            {
                Token = GenerateJwtToken(appUser).Result,
            });
        }

        public async Task<string> GenerateJwtToken(User user)
        {
            List<Claim> claims = new()
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.UserName)
            };

            IEnumerable<Claim> customClaims = _userManager.GetClaimsAsync(user).Result;

            foreach (var customClaim in customClaims)
            {
                claims.Add(new Claim(customClaim.Type, customClaim.Value));
            }

            claims.Add(new Claim(type: "Id", value: user.Id.ToString()));   //For setting CreatedBy, UpdatedBy and DeletedBy (post, put, patch and delete) 

            var roles = await _userManager.GetRolesAsync(user);

            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.GetSection("SSK").Value));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(100),
                SigningCredentials = creds
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}
