﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using DevTask.DataAccess.Infrastructure.IInfrastructure;
using AutoMapper;
using DevTask.ActionResults;
using System.Net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using DevTask.Dtos.Product;
using DevTask.Models;
using DevTask.GetParams;

namespace DevTask.Controllers.api
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class ProductsController : BaseController
    {
        public ProductsController(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {

        }

        // POST: api/Products
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Route("")]
        [Authorize(Policy = "AddingProducts")]
        public ActionResult Post([FromForm] AddProductDto addProductDto)
        {
            Product product = _mapper.Map<Product>(addProductDto);

            _unitOfWork.Products.Add(product);

            AddProductCategories(product.Id, addProductDto.CategoriesIds);

            _unitOfWork.Commit();

            return Get(new ProductParams
            {
                Id = product.Id
            });
        }

        // GET: api/Products
        [HttpGet]
        [Route("")]
        [Authorize(Policy = "ReadingProducts")]
        public ActionResult Get([FromQuery] ProductParams productParams)
        {
            var entitiesList = _unitOfWork.Products.Get(productParams).GetAwaiter().GetResult();
           
            IEnumerable<GetProductDto> getProductDtos = _mapper.Map<IEnumerable<GetProductDto>>(entitiesList.FilteredData);

            return Ok(new ApiActionResult
            {
                StatusCode = HttpStatusCode.OK,
                TotalCount = entitiesList.TotalCount,
                FilteredCount = entitiesList.FilteredCount,
                Data = getProductDtos,
            });
        }

        // PUT: api/Products
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut]
        [Route("{id}")]
        [Authorize(Policy = "EditingProducts")]
        public ActionResult Put(Guid id, [FromForm] EditProductDto editProductDto)
        {
            if (id != editProductDto.Id)
            {
                return Redirect("/api/Errors/Error/400");
            }

            Product product = _unitOfWork.Products.Get(product => product.Id == id).GetAwaiter().GetResult().FilteredData.FirstOrDefault();

            if (product == null)
            {
                return Redirect("/api/Errors/Error/404");
            }

            product = _mapper.Map<Product>(editProductDto);

            _unitOfWork.Products.Update(product);

            DeleteProductCategories(product.Id);
            AddProductCategories(product.Id, editProductDto.CategoriesIds);

            _unitOfWork.Commit();

            return Get(new ProductParams
            {
                Id = product.Id
            });

        }

        // DELETE: api/Products/{id}
        [HttpDelete]
        [Route("{id}")]
        [Authorize(Policy = "DeletingProducts")]
        public ActionResult Delete(Guid id)
        {
            GetProductDto getProductDto = _mapper.Map<IEnumerable<GetProductDto>>(_unitOfWork.Products.Get(
                product => product.Id == (id != Guid.Empty ? id : product.Id),
                null, null, "ProductCategories,ProductCategories.Category")
                .GetAwaiter().GetResult().FilteredData).FirstOrDefault();

            if (getProductDto == null)
            {
                return Redirect("/api/Errors/Error/404");
            }

            _unitOfWork.Products.Delete(id);

            _unitOfWork.Commit();

            return Ok(new ApiActionResult
            {
                StatusCode = HttpStatusCode.OK,
                Data = getProductDto
            });
        }

        private void AddProductCategories(Guid productId, IEnumerable<Guid> categoriesIds)
        {
            IEnumerable<Category> categories = _unitOfWork.Categories.Get(new CategoryParams { Ids = categoriesIds.Distinct() }).GetAwaiter().GetResult().FilteredData;

            if (categories.Count() != categoriesIds.Distinct().Count())
                throw new Exception("548"); // Incorrect Foreign key(s) specified

            var productsCategories = new List<ProductCategory>();
            foreach (var categoryId in categoriesIds)
            {
                productsCategories.Add(new ProductCategory { Id = Guid.NewGuid(), CategoryId = categoryId, ProductId = productId });
            }
            _unitOfWork.ProductsCategories.Add(productsCategories);
        }

        private void DeleteProductCategories(Guid productId)
        {
            _unitOfWork.ProductsCategories.Delete(
                _unitOfWork.ProductsCategories.Get(pc => pc.ProductId == productId)
                .GetAwaiter().GetResult().FilteredData);
        }
    }
}
