﻿using AutoMapper;
using GCRM.ActionResults;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DevTask.ActionResults;
using DevTask.DataAccess.Infrastructure.IInfrastructure;
using DevTask.Dtos.Product;
using DevTask.GetParams;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace DevTask.Controllers
{
    [Route("[controller]")]
    public class ProductsController : BaseController
    {
        private readonly api.ProductsController _productsApi;

        public ProductsController(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
            _productsApi = new api.ProductsController(_unitOfWork, _mapper);
        }

        // GET: Products
        [HttpGet]
        [Route("")]
        [Authorize(Policy = "ReadingCategories")]
        public IActionResult Index()
        {
            return View();
        }

        // POST: Products   
        [HttpPost]
        [Route("")]
        [Authorize(Policy = "AddingProducts")]
        public IActionResult Create([FromForm] AddProductDto addProductDto)
        {
            _productsApi.Post(addProductDto);

            return Ok(new MvcActionResult(ActionType.Add, HttpStatusCode.OK));
        }

        //POST: Products/GetDT
        [HttpPost]
        [Route("GetDT")]
        [Authorize(Policy = "ReadingProducts")]
        public IActionResult GetDT()
        {
            JqueryDatatable jqueryDatatable = new(Request);

            ProductParams productParams = new()
            {
                Search = jqueryDatatable.Search,

                Name = jqueryDatatable.ColsSearchValues["Name"],
                Description = jqueryDatatable.ColsSearchValues["Description"],
                Price = float.TryParse(jqueryDatatable.ColsSearchValues["Price"], out float price) ? price : null,

                SortCol = jqueryDatatable.SortColumn,
                SortDir = jqueryDatatable.SortColumnDir,

                PageNumber = (jqueryDatatable.Start + jqueryDatatable.Length) / jqueryDatatable.Length,
                PageSize = jqueryDatatable.Length,
            };

            ApiActionResult apiActionResult = (ApiActionResult)((OkObjectResult)_productsApi.Get(productParams)).Value;

            IEnumerable<GetProductDto> getProductDtos = (IEnumerable<GetProductDto>)apiActionResult.Data;

            return Ok(new
            {
                data = getProductDtos,
                recordsFiltered = apiActionResult.TotalCount,
                recordsTotal = apiActionResult.TotalCount,
            });
        }

        // PUT: Products/{id}
        [HttpPut]
        [Route("{id}")]
        [Authorize(Policy = "EditingProducts")]
        public IActionResult Edit(Guid id, [FromForm] EditProductDto editProductDto)
        {
            _productsApi.Put(id, editProductDto);

            return Ok(new MvcActionResult(ActionType.Update, HttpStatusCode.OK));
        }

        //DELETE: Products/{id

        [HttpDelete]
        [Route("{id}")]
        [Authorize(Policy = "DeletingProducts")]
        public async Task<IActionResult> Delete(Guid id)
        {
            _productsApi.Delete(id);

            return Ok(new MvcActionResult(ActionType.Delete, HttpStatusCode.OK));
        }
    }

}
