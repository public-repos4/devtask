﻿using AutoMapper;
using GCRM.ActionResults;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
namespace DevTask.Controllers
{
    [Route("[controller]")]
    public class HomeController : BaseController
    {
        // GET: Products
        [HttpGet]
        [Route("/")]
        public IActionResult Index()
        {
            return View();
        }
    }
}
