﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using DevTask.DataAccess.Infrastructure.IInfrastructure;
using AutoMapper;

namespace DevTask.Controllers
{
    public class BaseController : Controller
    {
        protected readonly IUnitOfWork _unitOfWork;
        protected readonly IMapper _mapper;

        public BaseController(IUnitOfWork unitOfWork = null, IMapper mapper = null)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
    }
}
