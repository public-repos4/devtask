﻿using AutoMapper;
using GCRM.ActionResults;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DevTask.ActionResults;
using DevTask.DataAccess.Infrastructure.IInfrastructure;
using DevTask.Dtos.Category;
using DevTask.GetParams;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DevTask.Controllers
{
    [Route("[controller]")]
    public class CategoriesController : BaseController
    {
        private readonly api.CategoriesController _categoriesApi;

        public CategoriesController(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
            _categoriesApi = new api.CategoriesController(_unitOfWork, _mapper);
        }

        // GET: Categories
        [HttpGet]
        [Authorize(Policy = "ReadingCategories")]
        public IActionResult Index()
        {
            return View();
        }

        // POST: Categories   
        [HttpPost]
        [Route("")]
        [Authorize(Policy = "AddingCategories")]
        public IActionResult Create([FromForm] AddCategoryDto addCategoryDto)
        {
            _categoriesApi.Post(addCategoryDto);

            return Ok(new MvcActionResult(ActionType.Add, HttpStatusCode.OK));
        }

        //POST: Categories/GetDT
        [HttpPost]
        [Route("GetDT")]
        [Authorize(Policy = "ReadingCategories")]
        public IActionResult GetDT()
        {
            JqueryDatatable jqueryDatatable = new(Request);

            CategoryParams categoryParams = new()
            {
                Search = jqueryDatatable.Search,

                Name = jqueryDatatable.ColsSearchValues["Name"],

                SortCol = jqueryDatatable.SortColumn,
                SortDir = jqueryDatatable.SortColumnDir,

                PageNumber = (jqueryDatatable.Start + jqueryDatatable.Length) / jqueryDatatable.Length,
                PageSize = jqueryDatatable.Length,
            };

            ApiActionResult apiActionResult = (ApiActionResult)((OkObjectResult)_categoriesApi.Get(categoryParams)).Value;

            IEnumerable<GetCategoryDto> getCategoryDtos = (IEnumerable<GetCategoryDto>)apiActionResult.Data;

            return Ok(new
            {
                data = getCategoryDtos,
                recordsFiltered = apiActionResult.TotalCount,
                recordsTotal = apiActionResult.TotalCount,
            });
        }

        // Get: Categories/GetSelect
        [HttpGet]
        [Route("GetSelect")]
        public IActionResult GetSelect([FromQuery] CategoryParams categoryParams)
        {
            ApiActionResult apiActionResult = (ApiActionResult)((OkObjectResult)_categoriesApi.Get(categoryParams)).Value;

            IEnumerable<GetCategoryDto> getCategoryDtos = (IEnumerable<GetCategoryDto>)apiActionResult.Data;

            return Ok(new
            {
                data = getCategoryDtos.Select(category => new
                {
                    id = category.Id,
                    text = category.Name,
                    optHtml = "<span>" + category.Name + "</span>",
                    selectedOptHtml = "<span>" + category.Name + "</span>",
                }).ToList(),

                totalFilteredRecordsCount = apiActionResult.TotalCount,
                thereIsMoreData = categoryParams.PageNumber * categoryParams.PageSize < apiActionResult.TotalCount
            });
        }

        // PUT: Categories/{id}
        [HttpPut]
        [Route("{id}")]
        [Authorize(Policy = "EditingCategories")]
        public IActionResult Edit(Guid id, [FromForm] EditCategoryDto editCategoryDto)
        {
            _categoriesApi.Put(id, editCategoryDto);

            return Ok(new MvcActionResult(ActionType.Update, HttpStatusCode.OK));
        }

        // DELETE: Categories/{id}
        [HttpDelete]
        [Route("{id}")]
        [Authorize(Policy = "DeletingCategories")]
        public async Task<IActionResult> Delete(Guid id)
        {
            _categoriesApi.Delete(id);

            return Ok(new MvcActionResult(ActionType.Delete, HttpStatusCode.OK));
        }
    }
}
