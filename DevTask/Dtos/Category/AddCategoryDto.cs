﻿using System;
using DevTask.Dtos.BaseEntity;
using System.ComponentModel.DataAnnotations;

namespace DevTask.Dtos.Category
{
    public class AddCategoryDto : AddBaseEntityDto
    {
        [Required(ErrorMessage = SharedDataAnnotationErrorMessage.RequiredErrorMessage)]
        public string Name { get; set; }
    }
}
    