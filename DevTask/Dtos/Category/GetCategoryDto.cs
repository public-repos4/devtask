﻿using DevTask.Dtos.BaseEntity;

namespace DevTask.Dtos.Category
{
    public class GetCategoryDto : GetBaseEntityDto
    {
        public string Name { get; set; }
    }
}