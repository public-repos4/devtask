﻿using System;

namespace DevTask.Dtos.BaseEntity
{
    public class EditBaseEntityDto
    {
        public Guid Id { get; set; }
    }
}
