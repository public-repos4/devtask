﻿using System;
using DevTask.Dtos.Identity;

namespace DevTask.Dtos.BaseEntity
{
    public class AddBaseEntityDto
    {
        private Guid? _id;

        public AddBaseEntityDto()
        {
            _id = Guid.NewGuid();
        }

        public Guid? Id
        {
            get => _id;
            set => _id = (value == null || value == Guid.Empty) ? new Guid() : value;
        }
    }
}
