﻿using System;
using DevTask.Dtos.Identity;

namespace DevTask.Dtos.BaseEntity
{
    public class GetBaseEntityDto
    {
        public Guid Id { get; set; }
    }
}
