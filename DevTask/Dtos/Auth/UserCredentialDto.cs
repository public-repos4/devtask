﻿using System;
using System.ComponentModel.DataAnnotations;
using DevTask.DataAccess;
using Microsoft.AspNetCore.Http;

namespace DevTask.Dtos.Auth
{
    public class UserCredentialDto
    {
        [Required(ErrorMessage =  SharedDataAnnotationErrorMessage.RequiredErrorMessage)]
        public string UserName { get; set; }

        [Required(ErrorMessage =  SharedDataAnnotationErrorMessage.RequiredErrorMessage)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
