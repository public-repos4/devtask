﻿using System;
using DevTask.Dtos.BaseEntity;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace DevTask.Dtos.Product
{
    public class AddProductDto : AddBaseEntityDto
    {
        private float _price;

        [Required(ErrorMessage = SharedDataAnnotationErrorMessage.RequiredErrorMessage)]
        public string Name { get; set; }

        [Required(ErrorMessage = SharedDataAnnotationErrorMessage.RequiredErrorMessage)]
        public string Description { get; set; }

        [Required(ErrorMessage = SharedDataAnnotationErrorMessage.RequiredErrorMessage)]
        [RegularExpression(SharedDataAnnotationRegexes.FloatRegex, ErrorMessage = SharedDataAnnotationErrorMessage.InvalidNumber)]
        [Range(float.Epsilon, float.MaxValue, ErrorMessage = SharedDataAnnotationErrorMessage.NotAllowedValue)]
        public float Price { get => _price; set => _price = value; }

        [Required(ErrorMessage = SharedDataAnnotationErrorMessage.RequiredErrorMessage)]
        public IEnumerable<Guid> CategoriesIds { get; set; }

    }
}
