﻿using DevTask.Dtos.BaseEntity;
using DevTask.Dtos.Category;
using DevTask.Models;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace DevTask.Dtos.Product
{
    public class GetProductDto : GetBaseEntityDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public float Price { get; set; }

        public virtual ICollection<GetCategoryDto> Categories { get; set; }

    }
}
