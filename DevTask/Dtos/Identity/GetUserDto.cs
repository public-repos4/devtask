﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DevTask.Dtos.BaseEntity;

namespace DevTask.Dtos.Identity
{
    public class GetUserDto : GetBaseEntityDto
    {
        //public string Picture { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        //public string MobileNumber { get; set; }
        //public bool IsActive { get; set; }

        //public IEnumerable<GetRoleDto> Roles { get; set; }
        //public IEnumerable<Guid> RolesIds { get; set; }
    }
}
