﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DevTask.Dtos.BaseEntity;

namespace DevTask.Dtos.Identity
{
    public class GetRoleDto : GetBaseEntityDto
    {
        public string Name { get; set; }
    }
}
