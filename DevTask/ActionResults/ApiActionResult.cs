﻿using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace DevTask.ActionResults
{
    public class ApiActionResult
    {
        public HttpStatusCode StatusCode { get; set; }
        public int TotalCount { get; set; }
        public int FilteredCount { get; set; }
        public object Data { get; set; }
    }
}
