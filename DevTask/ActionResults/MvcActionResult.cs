﻿using System.Globalization;
using System.Net;

namespace GCRM.ActionResults
{
    public enum ActionType
    {
        Add, Update, Delete
    }

    public class MvcActionResult
    {
        private string _message;
        public string Message
        {
            get => _message;
            set => _message = value;
        }

        public MvcActionResult(ActionType actionType, HttpStatusCode statusCode)
        {
            _message = actionType switch
            {
                ActionType.Add => "Added successfully",
                ActionType.Update => "Edited successfully",
                ActionType.Delete => "Deleted successfully",
                _ => "No result",
            };
        }
    }
}
