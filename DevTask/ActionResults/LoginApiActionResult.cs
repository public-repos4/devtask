﻿using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace DevTask.ActionResults
{
    public class LoginApiActionResult
    {
        public string Token { get; set; }
    }
}
