After you download the source code make sure to:
- download the .net core 5 sdk
- install and restor the required nuget packages
- create the required database (ex: FsitTask) and configure the connection string in appsitting.json file (preferd to use MSSQLSERVER 2017) 
- run update-database command using package manager console
- make sure that you are using the Development env
- run the project

you can test the project via postman app, the link of the collection for testing the apis:
note: make sure to call your apis using the same port number that your application use
link of the postman colletion apis:
https://www.getpostman.com/collections/f32e6fff15a51a1734c1